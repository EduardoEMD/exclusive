<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'exevent');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'DEp: RtwZ;`+/c/<df4/sj}bhKtM*pw.;<6zz4AHhbc;Qw+QmmSjaGbhw/`7@sxm');
define('SECURE_AUTH_KEY',  '<Vp;8mOs5K>Pcu8i8DBWd.vpNAL-m xOs5_k)[X3uSbnr72Fjrvl%WAPX7-Ai !#');
define('LOGGED_IN_KEY',    'S.C1pVtQj-s/^R@k<h8vWtCf52Bl*vjH-]r%E/B;JhC0km3<6tmR@cM:S7[)(F(`');
define('NONCE_KEY',        'qRxK[^$8?Z1WFgRt]xlCQ;aXo.FFmo~K$ZO2W.w+Mm!1k/!_Pwk(QG7aSsjgjP:p');
define('AUTH_SALT',        'u`-i.GA.<iH15c`J;Im_PC]vORS@R *Q],Wf@U03o2&VNZbE)KL9kFXUyj7f=Luo');
define('SECURE_AUTH_SALT', '-jUr)(*tC5fx5@E6fv~b%F484Sg!CySvLh5<K4Y9J-0pRB7Z_e,N>IOCFf~J@DK/');
define('LOGGED_IN_SALT',   '!sJ0.7xy^uUODX>alWEF0uy@f2<7U?g?Dl=`]qg!9iuZsg6^{&:}wLc6FJqj@~6a');
define('NONCE_SALT',       'u2ze=nleWP>Gzp`2Yu@YzfK^sApf!y#urmm0^hXi%W76s}0,{I+cQAm>eIibg0Ri');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
